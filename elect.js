// const { resolve } = require('path');
const { app, BrowserWindow } = require('electron');
// app.commandLine.appendSwitch('enable-unsafe-webgpu');

const createWindow = () => {
  const win = new BrowserWindow({
    width: 800,
    height: 600,
  })

  win.loadURL('http://localhost:3000/');
  // win.loadURL('http://localhost:3000/surma/index.html');
}

app.whenReady().then(() => createWindow())