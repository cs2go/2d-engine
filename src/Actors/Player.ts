import Ball from "../math/ball";
import Vector, { IVector } from "../math/vector";
import Actor from "../System/AI/Actor";
import { Controls, VerifiedControls } from "../System/AI/Controllers/bindings";
import Mouse from "../System/AI/Controllers/Mouse";
import Renderer from "../System/Renderer";

const speedLimit = {
	run: 3,
	walk: 1,
	crouch: 1,
}

class Player extends Actor {
	public velocity: IVector = Vector.create();
	private direction: IVector = Vector.create();

	protected controls: Controls = {
		fire: [0],
		walk: {
			modifyer: ['ShiftLeft'],
			north: ['KeyW'],
			south: ['KeyS'],
			east: ['KeyA'],
			west: ['KeyD'],
		},
		crouch: {
			modifyer: ['ControlLeft'],
			north: ['KeyW'],
			south: ['KeyS'],
			east: ['KeyA'],
			west: ['KeyD'],
		},
		run: {
			modifyer: [],
			north: ['KeyW'],
			south: ['KeyS'],
			east: ['KeyA'],
			west: ['KeyD'],
		}
	}

	get ball() {
		return new Ball(this.origin.x, this.origin.y, 10);
	}

	constructor() {
		super();
		// this.hook('fire', this.fire);
		this.hook('run', this.run);
		this.hook('walk', this.walk);
		this.hook('crouch', this.crouch);
	}

	private calcDir(movement?: VerifiedControls): IVector {
		let dir = Vector.create();

		if (movement?.north) dir = Vector.add(dir, { x: 0, y: -1 });
		if (movement?.south) dir = Vector.add(dir, { x: 0, y: 1 });
		if (movement?.east) dir = Vector.add(dir, { x: -1, y: 0 });
		if (movement?.west) dir = Vector.add(dir, { x: 1, y: 0 });

		return dir;
	}

	run = (movement?: VerifiedControls): void => {
		this.velocity = Vector.add(this.velocity, this.calcDir(movement));
	}

	walk = (movement?: VerifiedControls): void => {
		this.velocity = Vector.add(this.velocity, this.calcDir(movement));
		this.velocity = Vector.limit(this.velocity, speedLimit.walk);
	}

	crouch = (movement?: VerifiedControls): void => {
		this.velocity = Vector.add(this.velocity, this.calcDir(movement));
		this.velocity = Vector.limit(this.velocity, speedLimit.crouch);
	}

	fire = (): void => {
		// console.log('firing', Vector.angle(this.direction))
	}

	update() {
		this.velocity = Vector.limit(this.velocity, speedLimit.run);
		this.origin = Vector.add(this.origin, this.velocity);
		this.velocity = Vector.multiply(this.velocity, .85);

		const norm = Vector.normal(Vector.sub(Mouse.position, this.absolutePosition));
		const fullLen = Vector.multiply(norm, 15);

		this.direction = Vector.add(
			this.absolutePosition,
			fullLen
		);
	}

	render() {
		Renderer.renderBall(
			this.absolutePosition,
			10
		);

		Renderer.renderLine(
			this.absolutePosition,
			this.direction
		)
	}
}

export default Player;