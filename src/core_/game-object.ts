import Graphic from "../graphics/Graphic";

export default abstract class GameObject <T extends GameObject = any> extends Graphic<T> {
	abstract update(): void;
	protected _update() {
		this.update();

		this.children.forEach(
			child => child._update()
		)
	}
}