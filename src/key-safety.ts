window.addEventListener('keydown', e => {
	if(!e.ctrlKey) return; //	dont worry about other key combos just yet
	const ctrlKeys = ['s', 'w'];

	if (ctrlKeys.includes(e.key)) {
		e.preventDefault();
		e.stopPropagation();
	}
});