import Node from "./Node";
import Vector, { IVector } from "../math/vector";
import Renderer from "./Renderer";

interface Entity <C extends Entity = Entity<any>> {
	update?(): void;
	render?(): void;
}

/**
 * Just a collection of Modules assigned to the entity.
 * Most logic should be deferred to the actual module instance.
 */
abstract class Entity <C extends Entity = Entity<any>> extends Node<Entity, C> {
	private _origin = Vector.create();
	get origin(): IVector { return this._origin }
	set origin(v: IVector) { this._origin = v }

	visible: boolean = true;

	stroke: string = '#EA5455';
	fill: string = '#EA545566';

	get absolutePosition(): IVector {
		if (this.parent) return Vector.add(this.parent.absolutePosition, this.origin);
		else return this.origin;
	}

	protected _update(): void {
		this.update?.();
		this.children.forEach(child => child._update());
	}

	protected _render(): void {
		if (!this.visible) return;

		Renderer.stroke = this.stroke;
		Renderer.fill = this.fill;
		this.render?.();

		this.children.forEach(child => child._render());
	}
}

export default Entity;