import Vector, { IVector, Size } from "../../math/vector";

type IImage = {
	data: HTMLImageElement,
	hasLoaded: boolean
} | void;

export type CanvasTexture = (
	| string
	| CanvasGradient
	| CanvasPattern
)

export default abstract class Renderer {
	private static __screenSize: IVector = Vector.create();
	private static __canvas: HTMLCanvasElement;
	private static __ctx: CanvasRenderingContext2D;

	private static __images = new Map<string, IImage | void>();

	static get stroke(): CanvasTexture { return this.__ctx.strokeStyle }
	static set stroke(v: CanvasTexture) { this.__ctx.strokeStyle = v }

	static get fill(): CanvasTexture { return this.__ctx.fillStyle }
	static set fill(v: CanvasTexture) { this.__ctx.fillStyle = v }

	static with(canvas: HTMLCanvasElement | null): Renderer {
		this.__canvas = canvas as HTMLCanvasElement;
		this.__ctx = this.__canvas.getContext('2d') as CanvasRenderingContext2D;

		this.applyScreenSize();
		window.addEventListener('resize', this.applyScreenSize);

		return this;
	}

	static applyScreenSize = (): void => {
		const { width, height } = this.__canvas.getBoundingClientRect();
		this.__screenSize = Vector.create(width, height);
		this.__canvas.width = width;
		this.__canvas.height = height;
	}

	static getScreenSize = () => Vector.clone(this.__screenSize);


	static clearScreen(): void {
		const { x, y } = this.__screenSize;
		this.__ctx.clearRect(0, 0, x, y);
	}

	static renderImg(url: string, { x, y, w, h }: Size): void {
		if (this.__images.has(url)) {
			const image: IImage = this.__images.get(url);
			if (image && image.hasLoaded)
				this.__ctx.drawImage(
					image.data,
					x, y, w, h
				)
		} else {
			const image: IImage = {
				data: new Image(),
				hasLoaded: false
			};

			image.data.src = url;
			image.data.onload = () => image.hasLoaded = true;
			this.__images.set(url, image);
		}
	}

	static renderPoint(point: IVector): void {
		this.renderBall(point);
	}

	static renderBall(point: IVector = Vector.create(), radius: number = .5): void {
		this.__ctx.beginPath();
		this.__ctx.arc(point.x, point.y, radius, 0, 360);
		this.__ctx.stroke();
		this.__ctx.fill();
	}

	static renderLine(p1: IVector, p2: IVector): void {
		this.__ctx.beginPath();
		this.__ctx.moveTo(p1.x, p1.y);
		this.__ctx.lineTo(p2.x, p2.y);
		this.__ctx.closePath();
		this.__ctx.stroke();
	}

	static renderRect(p1: IVector, p2: IVector, p3: IVector, p4: IVector): void {
		this.__ctx.beginPath();

		this.__ctx.moveTo(p1.x, p1.y);
		this.__ctx.lineTo(p2.x, p2.y);
		this.__ctx.lineTo(p3.x, p3.y);
		this.__ctx.lineTo(p4.x, p4.y);
		this.__ctx.lineTo(p1.x, p1.y);
		this.__ctx.stroke();
		this.__ctx.fill();
	}

	static renderCoord(point: IVector): void {
		this.__ctx.strokeStyle = 'red';
		this.renderLine(
			Vector.add(point, { x: 0, y: -5 }),
			Vector.add(point, { x: 0, y: 5 })
		)

		this.__ctx.strokeStyle = 'blue';
		this.renderLine(
			Vector.add(point, { x: -5, y: 0 }),
			Vector.add(point, { x: 5, y: 0 })
		);
	}
}