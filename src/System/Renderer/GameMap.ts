import Point from "../../math/point";
import Line from "../../math/line";
import Ball from "../../math/ball";

import GameObject from "../../core_/game-object";
import Vector, { IVector } from "../../math/vector";
// import Player from '../actors/human-player';

// import Bullet
// import Crate
// import Wall
// import RoundWall

const validMapErr = (prop: string) => (
	`You must provide a '${prop}' property to be a valid map`
);

export default abstract class GameMap extends GameObject<GameMap> {
	private player: Player;
	public debugMode = true;
	public testPoints: { contact: IVector, age: number }[] = [];


	constructor() {
		super();
		Promise.resolve().then(() => GameMap.validateMap(this));
	}

	addTestPoints(...contactPoints: IVector[]) {
		contactPoints.forEach(contact =>
			this.testPoints.push({ contact, age: 50 })
		);
	}

	get offset(): IVector {
		if (!this.player) return super.offset;
		return Vector.sub(super.offset, this.player.position);
	}

	//	shortcut the inherited root functions
	//	the map itself is the root.
	get root() {
		return this;
	}

	static validateMap(map) {
		if (!map.size) throw validMapErr('size');
		if (!map.spawn) throw validMapErr('spawn');
	}

	get width() { return this.size.x }
	get height() { return this.size.y }

	setParent(parentGraphic) {
		this.__parent = parentGraphic;
	}

	spawnPlayer() {
		let spwanPos = new Point(
			Math.random() * (this.spawn.end.x - this.spawn.start.x - 50) + (this.spawn.start.x + 25),
			Math.random() * (this.spawn.end.y - this.spawn.start.y - 50) + (this.spawn.start.y + 25)
		);

		this.player = new Player(spwanPos);
		this.add(this.player);
	}

	get walls() {
		return this.children.filter(child =>
			child instanceof Wall ||
			child instanceof RoundWall ||
			child instanceof Crate
		);
	}

	get bullets() {
		return this.children.filter(child => child instanceof Bullet);
	}

	_render() {
		super._render();

		if (this.debugMode) {
			this.stroke = 'red';
			this.fill = 'rgba(50, 50, 255, .3)';
			this.testPoints = this.testPoints
				.filter(p => this.debugMode)
				.filter(p => p.age > 0)
				.filter(p => !!p.contact);

			this.testPoints.forEach(p => {
				if (this.debugMode) this.renderPoint(p.contact);
				p.age -= 1;
			});
		}
	}

	__playerWallCollisions() {
		this.walls
			.filter(wall => collision.test(
				this.player.position,
				wall.position
			))
			.forEach(wall => {
				let points = [];

				if (wall instanceof Wall) {
					var hits = collision.rectBall(wall.position, this.player.position);
				}
				else if (wall instanceof RoundWall) {
					var hits = collision.ballBall(this.player.position, wall.position);
				}
				else if (wall instanceof Crate) {
					var hits = collision.rectBall(wall.position, this.player.position);
				}

				if (this.debugMode) {
					if (hits instanceof Array) this.addTestPoints(...hits);
					else this.addTestPoints(hits);
				}

				if (hits instanceof Array) points.push(...hits);
				else points.push(hits);


				if (points.length) {
					let contact = collision.getClosest(this.player.position, points);
					if (!contact) return;

					let relativeContact = contact.clone().sub(this.player.position);
					let limitTo = this.player.position.radius - relativeContact.length;

					this.player.position.sub(relativeContact.limit(limitTo));
					this.player.velocity = new Point();
				}
			});
	}

	__bulletWallCollisions() {
		this.bullets.forEach(bullet => {
			if (bullet.hasHit) {
				this.remove(bullet);
				return;
			}

			let points = [];

			this.walls
				.filter(wall => collision.test(
					bullet.asLine,
					wall.position
				))
				.forEach(wall => {
					if (wall instanceof Wall) {
						var hits = collision.rectLine(wall.position, bullet.asLine);
					}
					else if (wall instanceof RoundWall) {
						var hits = collision.lineBall(bullet.asLine, wall.position);
					}

					if (hits instanceof Array) points.push(...hits);
					else points.push(hits);
				});

			if (points.length) {
				let contact = collision.getClosest(bullet.startPos, points);
				if (!contact) return;

				bullet.hasHit = contact;
			}
		});
	}

	_update() {
		super._update();

		this.__playerWallCollisions();
		this.__bulletWallCollisions();
	}
}