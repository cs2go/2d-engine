import Player from "../../Actors/Player";
import { rectBall } from "../../math/collision";
import Rect from "../../math/rect";
import Vector, { IVector } from "../../math/vector";
import Entity from "../Entity";
import Renderer from "../Renderer";

class Wall extends Entity {
	rect: Rect;

	constructor(x: number, y: number, w: number, h: number) {
		super();
		this.rect = Rect.fromWidthHeight(
			Vector.create(x, y),
			Vector.create(w, h)
		)

		const scale = 20;

		this.rect.p1 = Vector.multiply(this.rect.p1, scale);
		this.rect.p2 = Vector.multiply(this.rect.p2, scale);
		this.rect.p3 = Vector.multiply(this.rect.p3, scale);
		this.rect.p4 = Vector.multiply(this.rect.p4, scale);
	}

	render() {
		this.fill = '#000';
		this.stroke = 'transparent';
		Renderer.renderRect(
			Vector.add(this.absolutePosition, this.rect.p1),
			Vector.add(this.absolutePosition, this.rect.p2),
			Vector.add(this.absolutePosition, this.rect.p3),
			Vector.add(this.absolutePosition, this.rect.p4)
		)
	}
}

export default class Map extends Entity {
	private player: Player;

	private walls: Set<Wall> = new Set();
	private collisions: IVector[] = [];

	constructor() {
		super();
		this.player = new Player();
		this.player.origin = Vector.create(1100, 1150);
	}

	private setScreenOnPlayer(): void {
		const po = Vector.multiply(this.player.origin, -1);
		const screen = Vector.multiply(Renderer.getScreenSize(), .5);
		this.origin = Vector.add(po, screen);
	}

	update() {
		this.setScreenOnPlayer();
		this.simulate();
	}

	simulate() {
		const player = this.player.ball;
		const touches: IVector[] = [];

		//	detect contact
		Array.from(this.walls).forEach(wall => {
			const test = rectBall(wall.rect, player);
			if (test.length === 1) touches.push(test[0]);
			else if (test.length) {
				const closest = test.reduce((shortest, next) => {
					const s = Vector.sub(shortest, this.player.origin);
					const n = Vector.sub(next, this.player.origin);

					if (Vector.magnitude(s) < Vector.magnitude(n)) return shortest;
					else return next;
				}, Vector.create(100000, 100000))
				touches.push(closest)
			}
		});

		this.collisions = touches;

		// push back player
		// this.collisions = touches.map(p => {
		// 	const delta = Vector.sub(p, this.player.origin);
		// 	const dist = Vector.magnitude(delta);
		// 	const dir = Vector.normal(delta);
		// })
		// touches.forEach(p => {
		// 	const mag = Vector.sub(p, this.player.origin);
		// 	const dir = Vector.normal(Vector.sub(p, this.player.origin));
		// })
	}

	_render() {
		super._render();

		this.fill = 'red';
		this.stroke = 'red';

		this.collisions.forEach(p => Renderer.renderCoord(Vector.add(this.origin, p)));
		this.collisions = [];
	}


	async loadMap(file: string): Promise<void> {
		const root = document.createElement('div');

		const resp = await fetch(`/maps/${file}.map`);
		const mapData = await resp.text();
		root.innerHTML = mapData;

		root.querySelectorAll('#walls .wall').forEach(e => {
			const wall = new Wall(
				parseFloat(e.getAttribute('x') ?? '0'),
				parseFloat(e.getAttribute('y') ?? '0'),
				parseFloat(e.getAttribute('width') ?? '0'),
				parseFloat(e.getAttribute('height') ?? '0'),
			)

			this.add(wall);
			this.walls.add(wall);
		})

		this.add(this.player);
	}
}