type ParserError = {
	expected: string[];
	at: number;
	in: string;
}

type ParserState = {
	subject: string;
	index: number;
	result: any;
	error?: ParserError;
}

export type Operation = (state: ParserState) => ParserState;
type Pattern = (...args: any[]) => Operation;
type Parser = (pattern: Operation, fn: (...args: any[]) => any) => Operation;

const unexpected = (state: ParserState, ...expected: string[]) => ({
	expected,
	at: state.index,
	in: state.subject
})


export const str: Pattern = (str: string): Operation => state => {
	if (state.error) return state;

	if (state.subject.startsWith(str, state.index)) {
		return {
			...state,
			result: str,
			index: state.index + str.length,
		}
	} else {
		return {
			...state,
			error: unexpected(state, str),
		}
	}
}

export const num: Pattern = (): Operation => {
	const regex = /^(\d+)?(\.\d+)?/;

	return state => {
		if (state.error) return state;

		const subject = state.subject.substring(state.index);
		const match = regex.exec(subject);

		if (!match) return {
			...state,
			error: unexpected(state, '0-9')
		}

		const [result] = match;

		return {
			...state,
			index: state.index + result.length,
			result: result,
		}
	}
}

export const sequence: Pattern = (...patterns: Operation[]) => state => {
	if (state.error) return state;

	let nextState = state;
	const result = [];

	for (let n of patterns) {
		const test = n(nextState);
		if (test.error) return test;
		result.push(test.result);

		nextState = {
			...state,
			index: test.index,
		}
	}

	return {
		...state,
		index: nextState.index,
		result,
	}
}

export const optional: Pattern = (pattern: Operation) => state => {
	if (state.error) return state;

	const nextState = pattern(state);
	if (nextState.error) return state;
	return nextState;
}


type FindMapFn = (op: Operation, i: number, arr: Operation[]) => ParserState;
type ContMapFn = (state: ParserState, i: number, arr: Operation[]) => boolean;

export const choice: Pattern = (...patterns: Operation[]) => {
	const findMap = (arr: Operation[], map: FindMapFn, continueCondition: ContMapFn) => {
		const mappedItems: ParserState[] = [];

		arr.find((item, index, arr) => {
			const mapValue = map(item, index, arr);
			mappedItems.push(mapValue);
			return continueCondition(mapValue, index, arr);
		});

		return mappedItems;
	}

	return state => {
		if (state.error) return state;

		const results = findMap(
			patterns,
			pattern => pattern(state),
			nextState => !nextState.error
		);

		const nextState = results.find(i => !i.error);

		if (!nextState) {
			const expected = results.map(s => s.error?.expected).flat(Infinity) as string[];

			return {
				...state,
				error: unexpected(state, ...expected)
			}
		}

		return nextState;
	}
}

export const many: Pattern = (pattern: Operation, minimum: number = 0) => state => {
	if (state.error) return state;

	let prevState = state;
	let nextState = state;
	let result = [];

	while (true) {
		if (nextState.index >= nextState.subject.length) break;

		prevState = nextState;
		nextState = pattern(nextState);
		if (nextState.error) break;
		result.push(nextState.result);
	}

	if (result.length < minimum) return nextState;
	return {
		...prevState,
		result,
	}
}

export const map: Parser = (pattern, fn: (result: any) => any) => parserState => {
	const nextState = pattern(parserState);
	if (nextState.error) return nextState;

	return {
		...nextState,
		result: fn(nextState.result),
	}
}

export const chain: Parser = (pattern, fn: (result: ParserState) => Operation | undefined) => parserState => {
	const nextState = pattern(parserState);
	if (nextState.error) return nextState;

	const op = fn(nextState);
	if (!op) return nextState;
	return op(nextState);
}

export const run = (subject: string, op: Operation): ParserState => {
	const init: ParserState = {
		subject,
		index: 0,
		result: []
	}

	return op(init);
}