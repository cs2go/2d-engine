import { choice, many, map, num, Operation, run, sequence, str } from "./parser";

const single = (char: string) => sequence(
	str(char),
	num()
)

const double = (char: string) => sequence(
	str(char),
	num(),
	str(" "),
	num()
)

const H = () => map(
	single('H'),
	([cmd, x]) => ({ cmd, x })
)

const V = () => map(
	single('V'),
	([cmd, y]) => ({ cmd, y })
)

const M = () => map(
	double('M'),
	([cmd, x, _, y]) => ({ cmd, x, y })
)

const ast = (): Operation => many(
	choice(
		M(),
		H(),
		V(),
	)
)

const result = run(
	"M66 57H52V49M68 57H81V47H82M69 75V73M69 75H68M69 75H82V72M65 75V73M65 75H66M65 75H52V72M52 61H46M52 61H57M52 61V62M85 57V50V51M85 57H86M85 57V51M90 57H88H95M90 57V58M90 57H95M90 61V60M90 61H89H95M90 61H95M65 61V71V68M65 61V68M65 61H59M52 49V43H36V45M52 49H50M46 61V60M46 61H42M84 47H85V48V45M75 61H69V71V68H82M77 61H83H82V68M95 39V33H85V39M95 39H94M95 39V41M85 39H92M85 39V42M95 43V45M95 45H94M95 45V51M85 45V44M85 45H92M95 51H85M95 51V57M95 57V61M95 61V64M86 68H95V66M86 68V61M86 68H82M86 61H87H85H86ZM82 68V70M65 68H52M52 68V70M52 68V64M36 55H46M36 55V53M36 55V57M46 55V58M46 55V53M36 49H46V51M36 49H48M36 49V47M36 49V51M36 59V61H40",
	ast()
)

console.log(result);