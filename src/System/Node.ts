export default abstract class Node <P extends Node = any, C extends Node = any> {
	private _parent: P | undefined = undefined;
	get parent(): P | undefined { return this._parent }
	set parent(v: P | undefined) {
		if (v === this._parent) return;
		this._parent?.remove(this);
		this._parent = v;
		this._parent?.add(this);
	}

	private _children: Set<C> = new Set();
	get children(): C[] { return Array.from(this._children) }

	get root(): Node {
		if (this._parent) return this._parent.root;
		else return this;
	}

	get path(): Node[] {
		const path: Node[] = [this];
		let parent: Node | undefined = this.parent;

		while (parent) {
			path.unshift(parent);
			parent = parent._parent;
		}

		return path;
	}

	add(child: C): boolean {
		if (this._children.has(child)) return false;
		if (child.parent !== this) {
			child.parent?.remove(child);
			child.parent = this;
		}
		this._children.add(child);
		return true;
	}

	remove(child: C): boolean {
		if (!this._children.has(child)) return  false;
		this._children.delete(child);
		return true;
	}
}
