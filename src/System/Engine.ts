import Entity from "./Entity";
import Renderer from "./Renderer";

export abstract class Module {
	abstract update(): void;
}

export default class Engine extends Entity {
	private running: boolean = false;

	stop(): void {
		this.running = false;
	}

	start(): void {
		this.running = true;
		this.tick()
	}

	render() {
		Renderer.clearScreen();
	}

	private tick = () => {
		this._update();
		this._render();

		//	queue up the next tick
		if (this.running) requestAnimationFrame(this.tick);
	}
}
