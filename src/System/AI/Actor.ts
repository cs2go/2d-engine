import Entity from "../Entity";

import {
	Controls,
	isBindingActive,
	isControlActive,
	KeyBinding,
	ModifiedControls,
	VerifiedControls,
} from "./Controllers/bindings";

type CtrlHookFn = (state?: VerifiedControls) => void;

export default abstract class Actor extends Entity {
	protected abstract controls: Controls;

	private ctrlHook: Map<string, CtrlHookFn> = new Map();

	hook(name: string, callback: CtrlHookFn): void {
		if (this.ctrlHook.has(name))
			throw new Error('name has already been hooked');

		this.ctrlHook.set(name, callback);
	}

	private invokeHook(name: string, state?: VerifiedControls): void {
		const hookFn = this.ctrlHook.get(name);
		hookFn?.(state);
	}

	protected override _update(): void {
		const controls = Object.keys(this.controls);

		controls.forEach(ctrl => {
			const props = Object.getOwnPropertyNames(this.controls[ctrl]);

			if (props.includes('modifyer')) {
				const state = isControlActive(<ModifiedControls>this.controls[ctrl]);
				if (state) {
					if (state === true) this.invokeHook(ctrl);
					else this.invokeHook(ctrl, state);
				}
			} else {
				const state = isBindingActive(<KeyBinding>this.controls[ctrl]);
				if (state) this.invokeHook(ctrl);
			}
		})

		super._update();
	}

	update(): void {
		console.log('forced to be called');
	}
}
