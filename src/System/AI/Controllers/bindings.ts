import Keyboard from "./Keyboard"
import Mouse from "./Mouse"

export type EventSource = (
	| HTMLElement
	| Window
)

type KeyOrientation = (
	| 'Left'
	| 'Right'
)

type ModifyKey = (
	| 'Control'
	| 'Shift'
	| 'Alt'
)

export type Modifyer = (
	| `${ModifyKey}${KeyOrientation}`
)

export const modifyers: Modifyer[] = [
	'AltLeft',
	'AltRight',
	'ShiftLeft',
	'ShiftRight',
	'ControlLeft',
	'ControlRight',
]

export type Keys = (
	| `Key${string}`
	| Modifyer
)

//	TODO: refine to only the active buttons on the connected mouse
export type MouseButton = number;

export type KeyBinding = (string | number)[];


export type ModifiedControls = {
	modifyer: KeyBinding;
	[name: string]: KeyBinding;
}

export type Controls = {
	[name: string]: KeyBinding | ModifiedControls;
}

export type VerifiedControls = {
	[name: string]: boolean;
}


export const isControlModifyerActive = (binding: Modifyer[]): boolean => {
	if (binding.length === 0) {
		return modifyers.every(key => !Keyboard.isDown(key));

	} else {
		return modifyers.every(key => {
			const included = binding.includes(key);
			const state = Keyboard.isDown(key);
			return included === state;
		});
	}
}

export const isBindingActive = (binding: KeyBinding): boolean => {
	if (binding.length === 0) return false;
	return binding.every(key => {
		switch (typeof key) {
			case 'string': return Keyboard.isDown(key);
			case 'number': return Mouse.isDown(key);
			default: return false;
		}
	})
}

export const isControlActive = (binding: KeyBinding | ModifiedControls): boolean | VerifiedControls => {
	if (Array.isArray(binding))
		return isBindingActive(<KeyBinding>binding);

	const { modifyer, ...otherKeys } = binding;
	const props = Object.keys(otherKeys);

	//	@ts-ignore
	if (isControlModifyerActive(modifyer)) {
		const ctrls: [string, boolean][] = props.map(
			key => [
				key,
				isBindingActive(binding[key])
			]
		);

		const anyActiveCtrls = ctrls.some((b) => b[1]);

		if (!anyActiveCtrls) return false;
		return Object.fromEntries(ctrls);
	}

	return false;
}