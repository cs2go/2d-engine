import { EventSource, Keys } from "./bindings";

export default class Keyboard {
	private static state: Map<string, boolean> = new Map();
	static debug: boolean = false;

	static connect(source: EventSource = window): void {
		source.addEventListener('keydown', (e: Event) => {
			if (this.debug) console.log('down', (<KeyboardEvent>e).code)
			this.keydown((<KeyboardEvent>e).code)
		});
		source.addEventListener('keyup', (e: Event) => {
			this.keyup((<KeyboardEvent>e).code)
		})

		source.onblur = () => {
			Array.from(this.state.keys())
				.forEach(key => this.state.set(key, false));
		}
	}

	static isDown(key: string): boolean {
		if (!this.state.has(key)) return false;
		return this.state.get(key) ?? false;
	}

	private static keydown(key: string): void {
		this.state.set(key, true)
	}

	private static keyup(key: string): void {
		this.state.set(key, false);
	}
}
