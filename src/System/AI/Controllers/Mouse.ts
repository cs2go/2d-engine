import Vector, { IVector } from "../../../math/vector";
import { EventSource } from "./bindings";

export default abstract class Mouse {
	private static state: Map<number, boolean> = new Map();

	private static __position: IVector = Vector.create();
	static get position(): IVector {
		return Vector.clone(this.__position);
	}

	static connect(source: EventSource = window) {
		source.addEventListener('mousemove', e => this.move(e as MouseEvent));
		source.addEventListener('wheel', e => this.wheel(e as WheelEvent));

		source.addEventListener('mousedown', e => this.mousedown((<MouseEvent>e).button));
		source.addEventListener('mouseup', e => this.mouseup((<MouseEvent>e).button));
	}

	static isDown(button: number): Boolean {
		if (!this.state.has(button)) return false;
		return this.state.get(button) ?? false;
	}

	static angle(point: IVector): number {
		return Vector.angle(Vector.sub(point, this.position));
	}

	//	update types here pls.
	private static move(e: MouseEvent): void {
		this.__position = Vector.create(e.clientX, e.clientY);
	}

	private static mousedown(button: number): void {
		this.state.set(button, true);
	}

	private static mouseup(button: number): void {
		this.state.set(button, false);
	}

	private static wheel(e: WheelEvent): void {
		// console.log('wheel change');
	}
}
