import Point from "../physics/point";
import Line from "../physics/line";
import Rect from "../physics/rect";

// import GameMap
// import GameObject

// import Wall
// import RoundWall
// import Crate


module.exports = class DE_DUST extends GameMap {
	constructor() {
		super();

		this.size = new Point(1000, 1000);
		this.spawn = new Line(new Point(4220, 1390), new Point(4606, 2046));

		this.sites = {
			"A": new Line(new Point(5420, 1260), new Point(5770, 1650))
		}

		this.initWalls();
		this.initObstacles();

		this.spawnPlayer();
	}

	render() {
		this.renderImg('maps/de_dust.jpg', 0, 0, 7000, 7000);

		this.fill = 'transparent';
		this.stroke = 'orange';
		this.renderRect(Rect.fromLine(this.spawn));

		this.fill = 'rgba(255, 150, 150, .1)';
		this.stroke = 'red';
		this.renderRect(Rect.fromLine(this.sites['A']));

		//	test line ball intersection
		// Promise.resolve()
		// 	.then(() => {
		// 		this.stroke = 'red';
		// 		this.renderLine(this.lineTo);
		// 		this.renderPoint(this.intersect);

		// 		if (this.intersect.entry)
		// 			this.renderPoint(this.intersect.entry);

		// 		if (this.intersect.exit)
		// 			this.renderPoint(this.intersect.exit);
		// 	});
	}

	//	describes the objects in the map
	initObstacles() {
		//	ninja
		this.add(new Crate(4710, 930, 170, 120));
		this.add(new Crate(4860, 970, 90, 60));

		//	pizza
		this.add(new Crate(5069, 969, 100, 50));

		//	A SITE
		//	boxes
		this.add(new Crate(5370, 1220, 100, 110, 15));
		this.add(new Crate(5369, 1363, 100, 110));
		//	safe
		this.add(new Crate(5666, 1288, 100, 110));

		//	A short
		window.test = new Crate(4466, 2088, 120, 110);
		this.add(window.test);

		//	Cat boost
		this.add(new Crate(4320, 3061, 225, 70));

		//	cat
		this.add(new Crate(3678, 2737, 150, 100));

		//	palm
		this.add(new Crate(3456, 4316, 60, 60, -35));

		// this.add(new Crate(0, 0));
		// this.add(new Crate(0, 0));
	}

	//	descibes the walls for the map
	initWalls() {
		// this.add(new RoundWall(this.width, this.height, 100));

		this.testWall = new RoundWall(4090, 1095, 130)

		//	left A SHORT
		this.add(this.testWall);
		this.add(new RoundWall(4105, 2110, 130));
		this.add(new Wall(new Rect(
			new Point(4180, 1117),
			new Point(4190, 1117),
			new Point(4190, 2715),
			new Point(4180, 2715)
		)));

		//	top CAT
		this.add(new RoundWall(4050, 2730, 140));
		this.add(new RoundWall(3445, 2730, 140));
		this.add(new Wall(new Rect(
			new Point(3550, 2720),
			new Point(3940, 2720),
			new Point(3940, 2730),
			new Point(3550, 2730)
		)));

		//	bottom CAT
		this.add(new Wall(new Rect(
			new Point(3740, 3140),
			new Point(4556, 3140),
			new Point(4556, 3150),
			new Point(3740, 3150)
		)));

		//	left CATWALK
		this.add(new Wall(new Rect(
			new Point(3410, 2845),
			new Point(3430, 2845),
			new Point(3430, 4047),
			new Point(3410, 4047)
		)));


		//	right CATWALK
		this.add(new RoundWall(3740, 3280, 140));
		this.add(new Wall(new Rect(
			new Point(3600, 3280),
			new Point(3610, 3280),
			new Point(3610, 4335),
			new Point(3600, 4335)
		)));

		//	right PALM CORNER
		this.add(new RoundWall(3740, 4335, 140));

		//	right STAIRS
		this.add(new Wall(new Rect(
			new Point(4595, 2060),
			new Point(4605, 2060),
			new Point(4605, 3050),
			new Point(4595, 3050)
		)));
		this.add(new Wall(new Rect(
			new Point(4595, 2060),
			new Point(4820, 2060),
			new Point(4820, 2075),
			new Point(4595, 2075)
		)));

		//	right A SHORT
		this.add(new Wall(new Rect(
			new Point(4625, 1500),
			new Point(4645, 1500),
			new Point(4645, 2060),
			new Point(4625, 2060)
		)));

		//	bottom A PLAT 5394, 1670
		this.add(new Wall(new Rect(
			new Point(4625, 1500),
			new Point(5415, 1500),
			new Point(5415, 1520),
			new Point(4625, 1520)
		)));
		this.add(new Wall(new Rect(
			new Point(5395, 1500),
			new Point(5415, 1500),
			new Point(5415, 1670),
			new Point(5395, 1670)
		)));
		this.add(new Wall(new Rect(
			new Point(5395, 1650),
			new Point(5790, 1650),
			new Point(5790, 1670),
			new Point(5395, 1670)
		)));

		//	right A PLAT
		this.add(new Wall(new Rect(
			new Point(5770, 880),
			new Point(5790, 880),
			new Point(5790, 1670),
			new Point(5770, 1670)
		)));

		//	top right NINJA
		this.add(new Wall(new Rect(
			new Point(4095, 860),
			new Point(4105, 860),
			new Point(4105, 980),
			new Point(4095, 980)
		)));
		this.add(new Wall(new Rect(
			new Point(4095, 860),
			new Point(4910, 860),
			new Point(4910, 870),
			new Point(4095, 870)
		)));

		//	top PIZZA
		this.add(new Wall(new Rect(
			new Point(4900, 870),
			new Point(4910, 870),
			new Point(4910, 950),
			new Point(4900, 950),
		)));
		this.add(new Wall(new Rect(
			new Point(4900, 940),
			new Point(5380, 940),
			new Point(5380, 950),
			new Point(4900, 950),
		)));
	}

}