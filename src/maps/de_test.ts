import Map from "../System/Map/Map";
import Room from "../House/Room";
import Player from "../Actors/Player";

export default class extends Map {
	constructor() {
		super();
		this.addPlayer(new Player());
		this.add(new Room());
	}

	deconstruct(): void {
		this.remove(this.player);
	}
}