import Vector, { IVector } from "../math/vector";
import Entity from "../System/Entity";
import Renderer from "../System/Renderer";

const roomRnederLibrary: { [name: number]: IVector } = {
	0: Vector.create(200, 140),
	1: Vector.create(80, 140),
	2: Vector.create(140, 220),
	3: Vector.create(180, 100),
}

export default class Room extends Entity {
	origin: IVector = Vector.create(300, 400);

	private room: number;

	constructor(room: number = 3) {
		super();
		this.room = room;
	}

	render(): void {
		const room = roomRnederLibrary[this.room];

		Renderer.renderRect(
			Vector.add(this.origin, Vector.create(room.x * -.5, room.y * -.5)),
			Vector.add(this.origin, Vector.create(room.x * -.5, room.y * .5)),
			Vector.add(this.origin, Vector.create(room.x * .5, room.y * .5)),
			Vector.add(this.origin, Vector.create(room.x * .5, room.y * -.5)),
		)
	}
}