import { deg2rad, rad2deg } from './angles';

export type IVector = {
	x: number;
	y: number;
}

export type Size = IVector & {
	w: number;
	h: number;
}

// export type IScalar = IVector | number;

export default abstract class Vector {
	/**
	 * Construct a new IVector
	 */
	static create(x = 0, y = 0): IVector {
		return { x, y }
	}

	/**
	 * Returns the distance from (0|0) -> (vec.x|vec.y)
	 */
	static magnitude(vec: IVector): number {
		return Math.sqrt(
			(vec.x * vec.x) +
			(vec.y * vec.y)
		);
	}

	/**
	 * Returns a new vector that is of magnitude 1, but maintains the angle.
	 */
	static normal(vec: IVector): IVector {
		const length: number = Vector.magnitude(vec);

		if (length === 0) return { x: 0, y: 0 }

		return Vector.create(
			vec.x == 0 ? 0 : vec.x / length,
			vec.y == 0 ? 0 : vec.y / length
		);
	}

	/**
	 * Returns the angle of the supplied vector (in degrees)
	 */
	//	TODO: apparently, this is wrong, at
	//	least when calculating the angle from
	//	player to mouse direction.
	static angle(vec: IVector): number {
		return rad2deg(Math.atan2(vec.y, vec.x));
	}

	/**
	 * Returns the distance between two Vectors.
	 */
	static distance(vec1: IVector, vec2: IVector): number {
		if (Vector.magnitude(vec1) > Vector.magnitude(vec2)) {
			return Vector.magnitude(Vector.sub(vec1, vec2));
		} else {
			return Vector.magnitude(Vector.sub(vec2, vec1));
		}
	}


	/**
	 * Returns a Vector which is the sum of the two supplied Vectors.
	 */
	static add(...vectors: IVector[]): IVector {
		return vectors.reduce(
			(prev, next) => ({
				x: prev.x + next.x,
				y: prev.y + next.y
			}),
			Vector.create()
		)
	}

	/**
	 * Returns a Vector which is the sum of the first supplied Vector and the inverse if the second.
	 */
	static sub(...vectors: IVector[]): IVector {
		return vectors.reduce(
			(prev, next) => ({
				x: prev.x - next.x,
				y: prev.y - next.y
			})
		)
	}

	/**
	 * Returns a Vector that is scaled by a factor of 'scalar' compared to the supplied Vector.
	 */
	static multiply(vec: IVector, scalar: number): IVector {
		return Vector.create(
			vec.x * scalar,
			vec.y * scalar
		);
	}

	/**
	 * Returns a Vector that is the fraction of the the supplied Vector.
	 */
	static divide(vec: IVector, scalar: number): IVector {
		return Vector.create(
			vec.x / scalar,
			vec.y / scalar
		);
	}

	/**
	 * Return the center position of a collection of points
	 */
	static centerOf(...vectors: (IVector | null)[]): IVector {
		const realVecs = vectors.filter(x => x) as IVector[];

		if (realVecs.length === 0) throw new TypeError('Requires at least 1 IVector to work');
		if (realVecs.length === 1) return realVecs[0];

		const total = realVecs.reduce((sum, vec) => Vector.add(sum, vec), Vector.create());
		return Vector.divide(total, realVecs.length);
	}

	/*
	 * returns the closest collision from a array,
	 * otherwise returns an empty array
	 */
	getClosest = (target: IVector, collisions: IVector[]): IVector | null => {
		const realCollisions = collisions.filter(x => x);
		if (realCollisions.length <= 0) return null;

		return realCollisions.reduce((prev, next) => {
			if (prev === next) return prev;

			const prev_dist = Vector.distance(prev, target);
			const next_dist = Vector.distance(next, target);

			if (prev_dist <= next_dist) return prev;
			return next;
		}, realCollisions[0]);
	}


	/**
	 * Returns a Vector whos magnitude is the size of the limit supplied.
	 */
	static limit(vec: IVector, limit: number): IVector {
		return Vector.magnitude(vec) > limit ?
			Vector.multiply(Vector.normal(vec), limit) :
			Vector.clone(vec);
	}

	/**
	 * Returns a Vector that is rotated by 90deg anticlockwise to the supplied Vector.
	 */
	static left(vec: IVector): IVector {
		return Vector.create(vec.x * -1, vec.y);
	}

	/**
	 * Returns a Vector that is rotated by 90deg clockwise to the supplied Vector.
	 */
	static right(vec: IVector): IVector {
		return Vector.create(vec.x, vec.y * -1);
	}

	/**
	 * Returns a Vector that is rotated by 180deg from the supplied Vector.
	 */
	static reverse(vec: IVector): IVector {
		return Vector.multiply(vec, -1);
	}

	// TODO: this seems noped
	/**
	 * Returns a new Vector that maintains the same magnitude but is facing the direction provided.
	 */
	static rotateTo(vec: IVector, angle: number): IVector {
		const len = Vector.magnitude(vec);
		const radians = deg2rad(angle);

		const cos = Math.cos(radians);
		const sin = Math.sin(radians);

		const nAngle = Vector.create(
			(cos * vec.x) + (sin * vec.y),
			(cos * vec.y) - (sin * vec.x)
		);

		return Vector.multiply(nAngle, len);
	}

	/**
	 * Returns a new Vector that maintains the same magnitude but is rotated clockwise by the angle provided.
	 */
	static rotateBy(vec: IVector, angle: number): IVector {
		return Vector.rotateTo(vec, Vector.angle(vec) + angle);
	}

	/**
	 * Returns a duplicate Vector.
	 */
	static clone(vec: IVector): IVector {
		return Vector.create(
			vec.x,
			vec.y
		);
	}

	/**
	 * Applies the values from the second vector to the first.
	 * @param subject the Vector to modify
	 * @param target the Vector to replicate
	 */
	static apply(subject: IVector, target: IVector): IVector {
		subject.x = target.x;
		subject.y = target.y;
		return subject;
	}

	static dotProduct(start: IVector, end: IVector, point: IVector): IVector {
		const delta = Vector.sub(end, start);

		const dotCalc = (
			((point.x - start.x) * delta.x) +
			((point.y - start.y) * delta.y)
		);

		const dotProduct = (
			dotCalc === 0 ? 0 : dotCalc / Math.pow(Vector.magnitude(delta), 2)
		);

		return Vector.create(
			start.x + (dotProduct * delta.x),
			start.y + (dotProduct * delta.y)
		);

		// if (!(point instanceof Point)) throw new TypeError('point must be of type Point')

		// let dotCalc = (
		// 	((point.x - this.start.x) * (this.end.x - this.start.x)) +
		// 	((point.y - this.start.y) * (this.end.y - this.start.y))
		// );

		// let dotProduct = 0;
		// if (dotCalc != 0)
		// 	dotProduct = dotCalc / Math.pow(this.length, 2);

		// return new Point(
		// 	this.start.x + (dotProduct * (this.end.x - this.start.x)),
		// 	this.start.y + (dotProduct * (this.end.y - this.start.y))
		// );
	}
}
