import Point from './point'
import Ball from './ball'
import Line from './line';
import Rect from './rect';
import Vector, { IVector } from './vector';


/*
 * detects a collision of a point and a ball (point with radius)
 * returns the point where the collision occured, or null
 */
export const pointBall = (point: IVector, ball: Ball): IVector | null => {
	const dist = Vector.distance(point, ball);
	if (dist < ball.radius) return Vector.clone(point);
	return null;
}

/*
 * detects a collision of a line and a point (with some buffer distance)
 * returns true if collision occured, otherwise false
 */
export const linePoint = (line: Line, point: IVector, buffer = .1): IVector | null => {
	const dist_start = Vector.distance(point, line.start);
	const dist_end = Vector.distance(point, line.end);
	const dist_sum = dist_start + dist_end;

	const len = line.length;

	if (dist_sum <= len + buffer && dist_sum <= len + buffer) return Vector.clone(point);
	return null;
}

/*
 * detects a collision of a line and a point with a radius
 * returns the point on the line if occured, or
 * returns false
 */
export const lineBall = (line: Line, ball: Ball): IVector[] => {
	const collisions: IVector[] = [];

	/**
	 * if the dot product is on its line (between start and end)
	 * and if the dot is inside the ball
	 */
	const dot = Vector.dotProduct(line.start, line.end, ball);

	if (linePoint(line, dot, 0) && pointBall(dot, ball)) {
		collisions.push(dot);

	} else {
		if (pointBall(line.start, ball)) collisions.push(Vector.clone(line.start));
		if (pointBall(line.end, ball)) collisions.push(Vector.clone(line.end));
	}

	// let length = Vector.distance(ball, dot);
	// let distanceToArc = Math.sqrt(ball.radius * ball.radius - length * length);

	// let entry = Vector.add(Vector.limit(Vector.sub(line.start, dot), distanceToArc), dot);
	// let exit = Vector.add(Vector.limit(Vector.sub(line.end, dot), distanceToArc), dot);

	// if (linePoint(line, entry)) collisions.push(entry);
	// else if (pointBall(line.start, ball)) collisions.push(Vector.clone(line.start));

	// if (linePoint(line, exit)) collisions.push(exit);
	// else if (pointBall(line.end, ball)) collisions.push(Vector.clone(line.end));

	return collisions;
}


/*
 * detects a collision between two lines,
 * returns a point where the collision occured, or
 * returns false for no collision
 */
export const lineLine = (l1: Line, l2: Line): IVector | null => {
	let uA = (
		(l2.end.x - l2.start.x) *
		(l1.start.y - l2.start.y) -
		(l2.end.y - l2.start.y) *
		(l1.start.x - l2.start.x)
	) / (
			(l2.end.y - l2.start.y) *
			(l1.end.x - l1.start.x) -
			(l2.end.x - l2.start.x) *
			(l1.end.y - l1.start.y)
		);

	let uB = (
		(l1.end.x - l1.start.x) *
		(l1.start.y - l2.start.y) -
		(l1.end.y - l1.start.y) *
		(l1.start.x - l2.start.x)
	) / (
			(l2.end.y - l2.start.y) *
			(l1.end.x - l1.start.x) -
			(l2.end.x - l2.start.x) *
			(l1.end.y - l1.start.y)
		);

	let intersect = (uA >= 0 && uA <= 1 && uB >= 0 && uB <= 1);
	if (!intersect) return null;

	return Vector.create(
		l1.start.x + (uA * (l1.end.x - l1.start.x)),
		l1.start.y + (uA * (l1.end.y - l1.start.y))
	);
}

/*
 * detects a collision between two balls
 * returns true when collisions occured, otherwise false
 */
export const ballBall = (ball1: Ball, ball2: Ball): IVector | null => {
	const dist = Vector.distance(ball1, ball2);
	if (dist <= ball1.radius + ball2.radius) {
		return Vector.multiply(
			Vector.sub(ball2, ball1),
			.5
		)
	}

	return null;
}


/*
 * detects a collision between a ball and a rectangle
 * returns the point that the collision occured, otherwise false
 */
export const rectBall = (rect: Rect, ball: Ball): IVector[] => {
	let tests = [
		lineBall(rect.line1, ball),
		lineBall(rect.line2, ball),
		lineBall(rect.line3, ball),
		lineBall(rect.line4, ball)
	].flat(Infinity);

	return tests.filter(x => x) as IVector[];
}


/*
 * detects a collision between a rectangle and a line
 * returns an array of collisions, otherwise returns an empty array
 */
export const rectLine = (rect: Rect, line: Line): IVector[] => {
	let tests = [
		lineLine(rect.line1, line),
		lineLine(rect.line2, line),
		lineLine(rect.line3, line),
		lineLine(rect.line4, line)
	].flat(Infinity);

	return tests.filter(x => x) as IVector[];
}


/*
 * Return a simple true/false
 * if the items are close enough together to test
 * more intense physics
 */
type PhysicsObject = (
	| Point
	| Ball
	| Line
	| Rect
)
export const test = (obj1: PhysicsObject, obj2: PhysicsObject) => {
	const a1 = obj1.area;
	const a2 = obj2.area;

	//	object1 in area of object2
	if (a1.x > a2.w) return false;
	if (a1.w < a2.x) return false;
	if (a1.y > a2.h) return false;
	if (a1.h < a2.y) return false;

	return true;
}

// module.exports = Object.freeze({
// 	getClosest,
// 	test, physicsAreaOf,

// 	//	ball v ball
// 	ballBall,

// 	//	line v ball
// 	lineBall,

// 	//	point v point
// 	//	no op

// 	// point v ball
// 	pointBall,

// 	// point v rect
// 	// no op

// 	//	line v point
// 	linePoint,

// 	//	line v line
// 	lineLine,

// 	//	rect v ball
// 	rectBall,

// 	//	rect v line
// 	rectLine
// });
