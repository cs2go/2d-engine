const degPICalc = 180 / Math.PI;
export const rad2deg = (radians: number): number => radians * degPICalc;
export const deg2rad = (degrees: number): number => degrees / degPICalc;
