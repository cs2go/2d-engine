import Point from './point';
import Line from './line';
import Vector, { IVector } from '../math/vector';

export default class Rect {
	public p1: IVector;
	public p2: IVector;
	public p3: IVector;
	public p4: IVector;

	constructor(p1 = Vector.create(0, 0), p2 = Vector.create(1, 0), p3 = Vector.create(1, 1), p4 = Vector.create(0, 1)) {
		this.p1 = p1;
		this.p2 = p2;
		this.p3 = p3;
		this.p4 = p4;
	}

	get line1() {
		return new Line(this.p1, this.p2);
	}

	get line2() {
		return new Line(this.p2, this.p3);
	}

	get line3() {
		return new Line(this.p3, this.p4);
	}

	get line4() {
		return new Line(this.p4, this.p1);
	}

	get leftBound() {
		return Math.min(Math.min(this.p1.x, this.p2.x), Math.min(this.p3.x, this.p4.x));
	}

	get rightBound() {
		return Math.max(Math.max(this.p1.x, this.p2.x), Math.max(this.p3.x, this.p4.x));
	}

	get topBound() {
		return Math.min(Math.min(this.p1.y, this.p2.y), Math.min(this.p3.y, this.p4.y));
	}

	get bottomBound() {
		return Math.max(Math.max(this.p1.y, this.p2.y), Math.max(this.p3.y, this.p4.y));
	}

	get center() {
		return Vector.centerOf(
			this.p1,
			this.p2,
			this.p3,
			this.p4
		)
	}

	get area() {
		return {
			x: this.leftBound,
			y: this.topBound,
			w: this.rightBound,
			h: this.bottomBound
		}
	}

	clone() {
		return new Rect(
			Vector.clone(this.p1),
			Vector.clone(this.p2),
			Vector.clone(this.p3),
			Vector.clone(this.p4),
		);
	}

	add(offset: IVector) {
		Vector.apply(this.p1, Vector.add(this.p1, offset));
		Vector.apply(this.p2, Vector.add(this.p2, offset));
		Vector.apply(this.p3, Vector.add(this.p3, offset));
		Vector.apply(this.p4, Vector.add(this.p4, offset));

		return this;
	}

	rotate(angle: number) {
		const offset1 = Vector.sub(this.p1, this.center);
		const rotated1 = Vector.rotateTo(offset1, angle);
		Vector.apply(this.p1, Vector.add(this.center, rotated1));

		const offset2 = Vector.sub(this.p2, this.center);
		const rotated2 = Vector.rotateTo(offset2, angle);
		Vector.apply(this.p2, Vector.add(this.center, rotated2));

		const offset3 = Vector.sub(this.p3, this.center);
		const rotated3 = Vector.rotateTo(offset3, angle);
		Vector.apply(this.p3, Vector.add(this.center, rotated3));

		const offset4 = Vector.sub(this.p4, this.center);
		const rotated4 = Vector.rotateTo(offset4, angle);
		Vector.apply(this.p4, Vector.add(this.center, rotated4));

		return this;

		// let np4 = this.p4.clone().sub(center);
		// np4.rotateBy(angle);
		// this.p4 = np4.add(center);

		//	not completely tested
		// debugger;`
		console.warn('Rect.prototype.rotate has not been completed yet and still shows bugs');
	}

	static fromWidthHeight(pos: IVector, size: IVector) {
		return new Rect(
			new Point(pos.x, pos.y),
			new Point(pos.x + size.x, pos.y),
			new Point(pos.x + size.x, pos.y + size.y),
			new Point(pos.x, pos.y + size.y)
		)
	}

	static fromPosSize(x: number, y: number, w: number, h: number) {
		return new Rect(
			new Point(x, y),
			new Point(x + w, y),
			new Point(x + w, y + h),
			new Point(x, y + h)
		);
	}

	static fromLine(line: Line) {
		return Rect.fromPosSize(
			line.start.x,
			line.start.y,
			line.delta.x,
			line.delta.y
		)
	}
}
