import { IVector, Size } from './vector';
import Point from './point';

type IBall = IVector & {
	radius: number;
}

export default class Ball extends Point implements IBall {
	public radius: number;

	static from(ball: IBall) {
		return new Ball(ball.x, ball.y, ball.radius);
	}

	constructor(x = 0, y = 0, radius = 25) {
		super(x, y);
		this.radius = radius;
	}

	get area(): Size {
		return {
			x: this.x - this.radius - 10,
			y: this.y - this.radius - 10,
			w: this.x + this.radius + 10,
			h: this.y + this.radius + 10,
		}
	}

	clone() {
		return new Ball(this.x, this.y, this.radius);
	}
}
