import Vector, { IVector, Size } from "../math/vector";

export default class Point implements IVector {
	public x: number;
	public y: number;

	constructor(x: number = 0, y: number = 0) {
		this.x = x;
		this.y = y;
	}

	static from(obj: IVector) {
		return new Point(obj.x, obj.y);
	}

	get length() {
		return Vector.magnitude(this);
	}

	get normal() {
		return Vector.normal(this);
	}

	get angle() {
		return Vector.angle(this);
	}


	get left() {
		return new Point(this.x * -1, this.y);
	}
	get right() {
		return new Point(this.x, this.y * -1);
	}

	get reverse() {
		return new Point(this.x * -1, this.y * -1);
	}

	get area(): Size {
		return {
			x: this.x - 10,
			y: this.y - 10,
			w: this.x + 10,
			h: this.y + 10
		}
	}


	limit(limit: number) {
		if (this.length > limit) {
			let nPoint = Vector.limit(this, limit);
			Vector.apply(this, nPoint);
		}

		return this;
	}


	add(point: Point) {
		let nPoint = Vector.add(this, point);
		Vector.apply(this, nPoint);
		return this;
	}

	sub(point: Point) {
		let nPoint = Vector.sub(this, point);
		Vector.apply(this, nPoint);
		return this;
	}

	divide(magnitude: number) {
		let nPoint = Vector.divide(this, magnitude);
		Vector.apply(this, nPoint);
		return this;
	}

	multi(magnitude: number) {
		let nPoint = Vector.multiply(this, magnitude);
		Vector.apply(this, nPoint);
		return this;
	}

	rotate(angle: number) {
		let nPoint = Vector.rotateTo(this, angle);
		Vector.apply(this, nPoint);
		return this;
	}

	rotateBy(angle: number) {
		let nPoint = Vector.rotateBy(this, angle);
		Vector.apply(this, nPoint);
		return this;
	}
}
