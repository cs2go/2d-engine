import Vector, { IVector, Size } from '../math/vector';

export default class Line {
	public start: IVector;
	public end: IVector;

	constructor(start = Vector.create(), end = Vector.create()) {
		this.start = start;
		this.end = end;
	}

	get length() {
		return Vector.distance(this.start, this.end);
	}

	get delta() {
		return Vector.sub(this.end, this.end);
	}

	get area(): Size {
		const lo_x = Math.min(this.start.x, this.end.x);
		const lo_y = Math.min(this.start.y, this.end.y);
		const hi_x = Math.max(this.start.x, this.end.x);
		const hi_y = Math.max(this.start.y, this.end.y);

		return {
			x: lo_x - 10,
			y: lo_y - 10,
			w: hi_x + 10,
			h: hi_y + 10
		};
	}

	rotate(angle: number) {
		const nPoint = Vector.rotateTo(this.delta, angle);
		Vector.apply(
			this.end,
			Vector.add(this.start, nPoint)
		);
		return this;
	}

	rotateBy(angle: number) {
		const nPoint = Vector.rotateBy(this.delta, angle);
		Vector.apply(
			this.end,
			Vector.add(this.start, nPoint)
		);
		return this;
	}

	clone() {
		return new Line(
			Vector.clone(this.start),
			Vector.clone(this.end)
		)
	}

	// reflect(line: Line) {
	// 	const out: IVector = { x: 0, y: 0 };

	// 	const vec = Vector.sub(line.end, line.start);
	// 	const norm = Vector.normal(vec);
	// 	const dot = Vector.dotProduct(line.start, line.end, norm);

	// 	// const vec = line.delta;
	// 	// const norm = this.delta.normal;
	// 	// const dot = Vector.dotProduct(line.start, line.end, this.start);

	// 	// out.x = line.x - 2 * this.dotProductOf()
	// }

	dotProductOf(point: IVector) {
		return Vector.dotProduct(this.start, this.end, point);
	}
}
