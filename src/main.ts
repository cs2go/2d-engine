import './key-safety'
import './style.css'

import Engine from './System/Engine'

import Keyboard from './System/AI/Controllers/Keyboard'
import Mouse from './System/AI/Controllers/Mouse'
import Renderer from './System/Renderer'
import Map from './System/Map/Map'

Keyboard.connect();
Mouse.connect();
Renderer.with(document.querySelector('canvas'));

const engine = new Engine();
const map = new Map();
map.loadMap('de_test');
engine.add(map);
engine.start();
